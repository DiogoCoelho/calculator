import org.example.Calculator;
import org.testng.annotations.Test;
import static org.junit.Assert.*;

public class CalculatorTest {
    @Test
    public void testAdd() {
        Calculator calculator = new Calculator();
        int result = calculator.add(2, 3);
        assertEquals(5, result);
    }
    @Test
        public void testSub(){
        Calculator calculator = new Calculator();
        int result = calculator.subtract(3,2);
        assertEquals(1,result);
    }
}
